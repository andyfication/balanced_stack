This is my repo for the Balanced Stack project.


I worked with Goldsmiths, University of London and Coursera to develop some online interactive learning tools. The Balanced Stack is among these.

The Balanced Stack is an interactive simulation made in HTML5,CSS3 and javaScript.

The plugin shows step by step how to use stack data structures to check if a html snippet of code is well formed or not.

