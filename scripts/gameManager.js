// Class which handles game states
class GameManager {

  constructor(stackValidator, player) {
    // HTML references
    this.html_text = $('#html-text');
    this.tag_item_text = $(".tag-item-text");
    this.shortFeedback = $('#stack-feedback');
    this.hrLine = $('#hr-feed');
    this.stack_list = $('#stack-list');
    this.stackDisplayText = $('#stack-display-text');
    this.render_container = $('#render-container');
    this.render_feedback = $('#render-feedback');
    this.render_image_sad = $('#render-image-sad');
    this.render_image_happy = $('#render-image-happy');
    this.render_text = $('#render-text');
    // Reference to the html tags
    this.tag_element = null;
    // Reference to the Stack object
    this.stackValidator = stackValidator;
    // Reference to the player
    this.player = player;
    // Keep track of the amount of tags
    this.tagCounter = 0;
    // Current tag
    this.currentTag = 0;
    // List of tags
    this.tagList = [];
    // List of tasks to perform for each html stream
    this.tasks = [];
    // Variable which stores if the html stream is valid
    this.isValidHtml = "false";
    // Variable to store the current html exercise 
    this.currentHtmlStream = "";
    // Variable to store the html to render
    this.htmlToRender = "";
  }

  // Reset the Game Manager
  reset() {
    this.player.reset();
    this.tagCounter = 0;
    this.currentTag = 0;
    this.render_container.css('display', 'none');
    this.removeStackVisNodes();
    // Remove Highlight random and reset buttons
    this.player.buttonReset.removeClass('activeButton');
    this.player.buttonRandomHtml.removeClass('activeButton');
  }
  // Reset Game Manager when generating a new random html 
  newHtml(stack) {
    // clear tag grid
    this.clearGrid();
    this.player.reset();
    this.stackValidator = stack;
    this.tag_element = null;
    this.tagCounter = 0;
    this.currentTag = 0;
    this.tagList = [];
    this.tasks = [];
    this.isValidHtml = "false";
    this.currentHtmlStream = "";
    this.htmlToRender = "";
    this.render_container.css('display', 'none');
    this.removeStackVisNodes();
    // Remove Highlight random and reset buttons
    this.player.buttonReset.removeClass('activeButton');
    this.player.buttonRandomHtml.removeClass('activeButton');
  }

  // Assign value to short feedback 
  assignValueToFeedback(text) {
    this.shortFeedback.html(text);
  }

  // Get next tag
  nextTag() {
    this.currentTag += 1;
  }


  // Function which generates a random html stream taken from a list
  generateHtmlStream(random) {
    // Get a random ecercise 
    if (random) {
      // Get a random html string from the exercise list
      let randomPick = Math.floor(Math.random() * htmlExercisesContainer.length);
      console.log(randomPick);
      this.currentHtmlStream = htmlExercisesContainer[randomPick];
      // Html to render 
      this.htmlToRender = htmlExercisesContainerClean[randomPick];
      // Get the tutorial exercise
    } else {
      this.currentHtmlStream = htmlTutorialExercise[0];
      // Html to render 
      this.htmlToRender = htmlTutorialExerciseClean[0];
    }

    // Split list and retrive array of separate sub strings
    let randomStringToArray = this.currentHtmlStream.split(" ");
    // String text to display as html
    let htmlStringText = "";
    // Keep track of the number of tags added to the grid list
    this.tagCounter = 0;

    // Loop the string array and check for tags
    for (let index = 0; index < randomStringToArray.length; index++) {
      // Check if it is a tag
      if (this.checkIfTag(randomStringToArray[index])) {
        // Add all the tags to the tagList
        this.tagList.push(randomStringToArray[index]);
        // Create a copy of the tag and transofrm it to text for html rendering
        let htmlToText = this.htmlTagToText(randomStringToArray[index]);
        // Check if we have already added the tag to the frid list 
        this.addTagsToGridList(htmlToText);
        // Check if the tag requires a new line
        if (this.checkIfTagNewline(randomStringToArray[index])) {
          // Add new line at the end
          htmlStringText += "<span class = 'tag-element'>" + htmlToText + "</span>" + "<br />";
        }
        // The tag does not require a new line
        else {
          // Do not add new line at the end
          htmlStringText += " " + "<span class = 'tag-element'>" + htmlToText + "</span>" + " ";
        }
      }
      // It is not a tag but only text
      else {
        // Just add the text
        htmlStringText += " " + randomStringToArray[index] + " ";
      }
    }
    // Display the html text 
    this.html_text.html(htmlStringText);
    // Find all the tags just added
    this.tag_element = $('.tag-element');
  }

  // Helper Function
  // Convert html tags to plain text
  htmlTagToText(tag) {
    tag = tag.replace(/</g, "&lt");
    tag = tag.replace(/>/g, "&gt");
    return tag;
  }


  // Helper Function
  // Add tags to the grid list
  addTagsToGridList(htmlToText) {
    // Have we already added the same tag ? 
    let alreadyAddedTag = false;
    // Check if we have already added the tag
    for (let index = 0; index < this.tag_item_text.length; index++) {
      // Store the correct string match (html vs plain text fix)
      let match = $(this.tag_item_text[index]).html().replace(/;/g, "");
      // Already added
      if (match == htmlToText) {
        alreadyAddedTag = true;
        break;
      }
    }
    // If not added, then add the tag to the list 
    if (!alreadyAddedTag) {
      $(this.tag_item_text[this.tagCounter]).html(htmlToText);
      this.tagCounter += 1;
    }
  }

  // Clear the grid tag
  clearGrid() {
    // Check if we have already added the tag
    for (let index = 0; index < this.tag_item_text.length; index++) {
      $(this.tag_item_text[index]).html('');
    }
  }


  // Helper Function
  // Check if the element is a tag
  checkIfTag(element) {
    // list of html opening tags 
    let htmlOpen = ['<html>', '<head>', '<title>', '<body>',
      '<p>', '<div>', '<strong>', '<span>', '<ul>', '<ol>', '<li>', '<em>', '<h1>', '<h2>', '<h3>'
    ];
    // list of html closing tags
    let htmlClose = ['</html>', '</head>', '</title>', '</body>',
      '</p>', '</div>', '</strong>', '</span>', '</ul>', '</ol>', '</li>', '</em>', '</h1>', '</h2>', '</h3>'
    ];
    // Check if it is an opening tag
    for (let index = 0; index < htmlOpen.length; index++) {
      if (element == htmlOpen[index]) {
        return true;
      }
    }
    // Check if it is a closing tag
    for (let index = 0; index < htmlClose.length; index++) {
      if (element == htmlClose[index]) {
        return true;
      }
    }
    // If not a tag return false
    return false;
  }

  // Helper Function
  // Check if the element requires new line
  checkIfTagNewline(element) {
    // list of html opening tags which require new line 
    let htmlOpenNewline = ['<html>', '<head>', '<body>',
      '<div>', '<ul>', '<ol>'
    ];
    // list of html closing tags which require new line
    let htmlCloseNewline = ['</head>', '</title>', '</body>',
      '</p>', '</div>', '</ul>', '</ol>', '</li>', '</h1>', '</h2>', '</h3>'
    ];

    // Check if it is an opening tag and requires a new line 
    for (let index = 0; index < htmlOpenNewline.length; index++) {
      if (element == htmlOpenNewline[index]) {
        return true;
      }
    }
    // Check if it is a closing tag and requires a new line
    for (let index = 0; index < htmlCloseNewline.length; index++) {
      if (element == htmlCloseNewline[index]) {
        return true;
      }
    }
    // If not new line is required return false
    return false;
  }

  // Perform the balanced html algorithm with stack and create tasks
  validateHtmlStream() {
    // list of html opening tags 
    let htmlOpen = ['<html>', '<head>', '<title>', '<body>',
      '<p>', '<div>', '<strong>', '<span>', '<ul>', '<ol>', '<li>', '<em>', '<h1>', '<h2>', '<h3>'
    ];
    // list of html closing tags
    let htmlClose = ['</html>', '</head>', '</title>', '</body>',
      '</p>', '</div>', '</strong>', '</span>', '</ul>', '</ol>', '</li>', '</em>', '</h1>', '</h2>', '</h3>'
    ];
    // We can not pop if the stack is empty 
    let validPop = true;
    // Loop each value in the tagList
    for (let index = 0; index < this.tagList.length; index++) {
      let currentTag = this.tagList[index];
      // If it is an open tag, push it to the stack
      if (htmlOpen.includes(currentTag)) {
        this.stackValidator.push(currentTag);
        this.tasks.push("push");
        // If not check for matching and pop the stack
      } else if (htmlClose.includes(currentTag)) {
        // We can not pop if the size of the stack is 0. Means we have to many closing html tags.
        if (this.stackValidator.size > 0) {
          if (this.stackValidator.top.value == currentTag.replace('/', '')) {
            this.stackValidator.pop();
            this.tasks.push("pop");
          } else {
            // Char not matching 
            validPop = false;
            this.tasks.push("popNoMatch");
            break;
          }
        } else {
          // Stack is empty, can nit pop
          validPop = false;
          this.tasks.push("popEmptyStack");
          break;
        }
      }
    }
    // Reached the end of the task flag
    this.tasks.push("end");
    // Html stream validation
    if (this.stackValidator.size == 0 && validPop == true) {
      this.isValidHtml = "true";
    } else {
      this.isValidHtml = "false";
    }
    console.log(this.tasks);
  }


  // Highlight the current tag to examine
  highlightTag(currentTagIndex) {
    for (let index = 0; index < this.tag_element.length; index++) {
      $(this.tag_element[index]).removeClass('highlight-tag');
    }
    $(this.tag_element[currentTagIndex]).addClass('highlight-tag');
  }

  // Remove all highlights
  removeHighlightTag() {
    for (let index = 0; index < this.tag_element.length; index++) {
      $(this.tag_element[index]).removeClass('highlight-tag');
    }
  }

  // Add a stack visual node to the list
  addStackVisNode(value) {
    let valToAdd = value;
    // Remove empty stack sign
    if (this.player.stack.size != 0) {
      console.log('Hey');
      $(this.stackDisplayText).css('display', 'none');
    }
    // Create new node with classes
    let node = document.createElement('li');
    node.className = 'top-node';
    let innerNode = document.createElement('span');
    innerNode.className = 'inner-node not-visible';
    let spanText = document.createTextNode(valToAdd);
    innerNode.append(spanText);
    node.append(innerNode);
    // Check if it is the first node added
    if (this.stack_list.children().length > 0) {
      this.stack_list.prepend(node);
    } else {
      this.stack_list.append(node);
    }

    // Update the li text visibility
    for (let index = 0; index < this.stack_list.find("li").length; index++) {
      this.stack_list.children().eq(index).first().className = 'inner-node not-visible';
    }

    // Update the li text visibility
    for (let index = 1; index < this.stack_list.find("li").length; index++) {
      this.stack_list.children().eq(index).removeClass('top-node');
      this.stack_list.children().eq(index).addClass('stack-node');
      this.stack_list.children().eq(index).children().eq(0).addClass('not-visible');

    }
    // Show the top text
    this.stack_list.children().eq(0).first().html('<span class = "inner-node">Top</span>');
  }

  //Show the top value
  showTop() {
    this.stack_list.children().eq(0).children().eq(0).removeClass('not-visible');
    let value = this.player.stack.peek();
    this.stack_list.children().eq(0).first().html('<span class = "inner-node">' + this.htmlTagToText(value) + '</span>');
  }

  // Remove the most recent added visual node from the stack
  removeStackVisNode() {
    $(this.stack_list).find('li').last().remove();
    // Remove empty stack sign
    if (this.player.stack.size == 0) {
      $(this.stackDisplayText).css('display', 'block');
    }
  }

  // Remove all nodes (used on reset and new random html)
  removeStackVisNodes() {
    $(this.stack_list).find('li').remove();
    $(this.stackDisplayText).css('display', 'block');
  }

  // Check when answering yes
  acceptHtmlAnswer(e) {
    // Display html rendering container
    this.render_container.css('display', 'block');
    // Highlight random and reset buttons
    this.player.buttonReset.addClass('activeButton');
    this.player.buttonRandomHtml.addClass('activeButton');
    // Activate buttons
    this.player.buttonReset.removeClass('non-active');
    this.player.buttonRandomHtml.removeClass('non-active');
    // The html has balanced tags
    if (e.target.value == this.isValidHtml) {
      // The stack was well managed 
      if (this.tasks[this.player.currentTask] == "end") {
        $('#suggestion').html('Well done, the html code is well formed');
        $('.answer').css('display', 'none');
        this.render_feedback.html('Great, you have correctly verified the html code and properly used the stack data structure. You deserve to see how the html renders.');
        this.render_image_sad.css('display', 'none');
        this.render_image_happy.css('display', 'none');
        this.render_text.css('display', 'block');
        this.render_text.html(this.htmlToRender);
        // The stack was not well managed 
      } else {
        $('#suggestion').html('The html is well formed but you need to use the stack to confirm it');
        $('.answer').css('display', 'none');
        this.render_feedback.html('Although the html is well formed, you did not use stack data structure correctly. I am afraid but the html cannot be rendered.');
        this.render_image_sad.css('display', 'block');
        this.render_image_happy.css('display', 'none');
        this.render_text.css('display', 'none');
      }
      // The html does not have balanced tags 
    } else {
      $('#suggestion').html('Not really, the html code is not well formed');
      $('.answer').css('display', 'none');
      this.render_feedback.html('No, the html is not well formed. Try this exercise one more time and submit the correct solution.');
      this.render_image_sad.css('display', 'block');
      this.render_image_happy.css('display', 'none');
      this.render_text.css('display', 'none');

    }
  }

  // Check when answering no
  rejectHtmlAnswer(e) {
    // Display html rendering container
    this.render_container.css('display', 'block');
    // Highlight random and reset buttons
    this.player.buttonReset.addClass('activeButton');
    this.player.buttonRandomHtml.addClass('activeButton');
    // Activate buttons
    this.player.buttonReset.removeClass('non-active');
    this.player.buttonRandomHtml.removeClass('non-active');
    // The html does not have balanced tags
    if (e.target.value == this.isValidHtml) {
      // The stack was well managed 
      if (this.tasks[this.player.currentTask] == "end") {
        $('#suggestion').html('Well done, the html code is not well formed');
        $('.answer').css('display', 'none');
        this.render_feedback.html('Great, you have correctly verified the html code and properly used the stack data structure. The html code is not well formed and cannot be rendered.');
        this.render_image_sad.css('display', 'none');
        this.render_image_happy.css('display', 'block');
        this.render_text.css('display', 'none');
        // The stack was not well managed 
      } else {
        $('#suggestion').html('The html is not well formed but you need to use the stack to confirm it');
        $('.answer').css('display', 'none');
        this.render_feedback.html('Although the html is not well formed, you did not use stack data structure correctly. I am afraid but the html cannot be rendered.');
        this.render_image_sad.css('display', 'block');
        this.render_image_happy.css('display', 'none');
        this.render_text.css('display', 'none');
      }
      // The html has balanced tags
    } else {
      $('#suggestion').html('Not really, the html code is well formed');
      $('.answer').css('display', 'none');
      this.render_feedback.html('No, the html is well formed. Try this exercise one more time and submit the solution.');
      this.render_image_sad.css('display', 'block');
      this.render_image_happy.css('display', 'none');
      this.render_text.css('display', 'none');

    }
  }
  // Undo the answer option and go back to the exercise
  undoHtmlAnswer() {
    $('.answer').css('display', 'none');
    currentTutorialCount = 11;
    tutorial[currentTutorialCount].set();
  }
}