// The class player controls the player moves
class Player {
  constructor() {
    // List of player button interaction 
    this.buttonTop = $('#top-button');
    this.buttonPop = $('#pop-button');
    this.buttonPush = $('#push-button');
    this.buttonIsEmpty = $('#empty-button');
    this.buttonReset = $('#reset-button');
    this.buttonRandomHtml = $('#random-button');
    this.buttonAnswer = $('.answer');
    // The stack constructed by the player 
    this.stack = new Stack();
    // Current list tag selected
    this.currentTagSelected = "-";
    // Current task to perform
    this.currentTask = 0;
  }

  // Reset the tag selection
  reset() {
    this.currentTask = 0;
    this.stack = new Stack();
    this.currentTagSelected = "-";
  }

  // Get next task
  nextTask() {
    this.currentTask += 1;
    console.log('called');
  }

  // Assign current selected tag 
  assignCurrentTag(tag) {
    // If empty selection 
    if (!tag)
      // Assign this symbol 
      this.currentTagSelected = "--";
    else
      // Valid tag
      this.currentTagSelected = tag;
  }
}