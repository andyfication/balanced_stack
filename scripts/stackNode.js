// Node structure for the Stack 
class StackNode {
  constructor(value, next) {
    this.value = value;
    this.nextNode = next;
  }
}