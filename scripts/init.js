//Main Javascript function. Handles the program 
$(document).ready(() => {
  init_app();
});

//This will be used for the coursera API
courseraApi.callMethod({
  type: "GET_SESSION_CONFIGURATION",
  onSuccess: init_app
});

// Initialise the App
function init_app() {
  // Wait for the game to begin before setup
  let mainApp = setInterval(() => {
    // Begin Game initialisation
    if (beginGame) {
      // Player reference
      player = new Player();
      // stack used to validate the html stream
      stackValidator = new Stack();
      // Game manager reference
      gameManager = new GameManager(stackValidator, player);
      // Are we coming from the tutorial ?
      if (beginTutorial) {
        // Generate tutorial html stream
        gameManager.generateHtmlStream(false);
      } else {
        // Generate a random html stream
        gameManager.generateHtmlStream(true);
      }
      // Validate the stream and create the player tasks
      gameManager.validateHtmlStream();
      // Highlight first tag in the html text 
      gameManager.highlightTag(gameManager.currentTag);
      // Only run the game setup once
      clearInterval(mainApp);

      // Event Handlers-------------------------------------------------------

      // Reset Current Html and empty the Stack------------
      player.buttonReset.click(function () {
        $('.answer').css('display', 'none');
        gameManager.reset();
        gameManager.assignValueToFeedback('Select one of the actions');
        gameManager.highlightTag(gameManager.currentTag);
        // clear all tag selections highlight
        for (let index = 0; index < gameManager.tag_item_text.length; index++) {
          $(gameManager.tag_item_text[index]).parent().removeClass('current-selection');
        }
        currentTutorialCount = 10;
        tutorial[currentTutorialCount].set();
      });

      // Generate a new Random String and clear the Stack------------
      player.buttonRandomHtml.click(function () {
        $('.answer').css('display', 'none');
        stackValidator = new Stack();
        gameManager.newHtml(stackValidator);
        gameManager.generateHtmlStream(true);
        gameManager.validateHtmlStream();
        gameManager.assignValueToFeedback('Select one of the actions');
        gameManager.highlightTag(gameManager.currentTag);
        // clear all tag selections highlight
        for (let index = 0; index < gameManager.tag_item_text.length; index++) {
          $(gameManager.tag_item_text[index]).parent().removeClass('current-selection');
        }
        // Reset current game state 
        currentTutorialCount = 10;
        tutorial[currentTutorialCount].set();
      });

      // Grid Tag Selection-----------------------
      gameManager.tag_item_text.parent().click(function () {
        // We have reached the end of the task, no other actions are alowed
        if (gameManager.player.currentTask == gameManager.tasks.length - 1) {
          // Selection animation correct
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedback');
          }, 1000);
          gameManager.assignValueToFeedback('You reached the end of this task - Submit');
        } else {
          // clear all tag selections highlight
          for (let index = 0; index < gameManager.tag_item_text.length; index++) {
            $(gameManager.tag_item_text[index]).parent().removeClass('current-selection');
          }
          // add current selection
          $(this).addClass('current-selection');
          // Assign current selected tag to the player variable
          gameManager.player.assignCurrentTag($(this).find('span').text());
          // If the current selection is not empty
          if ($(this).find('span').html()) {
            // record the feedback and display
            gameManager.assignValueToFeedback('Your current selection is : ' + " ' " + $(this).find('span').html() + " '");
            // Selection animation correct
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedback');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedback');
            }, 1000);
            // Empty selection
          } else {
            // record the feedback and display
            gameManager.assignValueToFeedback('Your current selection is : ' + " ' " + 'Empty' + " '");
            // selection animation Wrong
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
            }, 1000);
          }
        }
      });

      // Push Button Click----------------------------
      gameManager.player.buttonPush.click(function () {
        // We have reached the end of the task, no other actions are alowed
        if (gameManager.player.currentTask == gameManager.tasks.length - 1) {
          // Selection animation correct
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedback');
          }, 1000);
          gameManager.assignValueToFeedback('You reached the end of this task - Submit');
        }
        // Perfom a possible push action only if the player selects the current tag to examine
        else if ($(gameManager.tag_element[gameManager.currentTag]).text() == player.currentTagSelected) {
          // Now we need to check if the task is a push or not
          if (gameManager.tasks[gameManager.player.currentTask] == "push") {
            // Selection animation correct
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedback');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedback');
            }, 1000);
            // Perform the push
            gameManager.player.stack.push(gameManager.player.currentTagSelected);
            // Display push message 
            gameManager.assignValueToFeedback('Tag ' + gameManager.htmlTagToText(gameManager.player.currentTagSelected) + " pushed to the stack");
            // Add node to the visual stack
            gameManager.addStackVisNode(gameManager.player.currentTagSelected);
            // Set the next task for the player
            gameManager.player.nextTask();
            // Only if we have tags left we move
            if (gameManager.currentTag < gameManager.tagList.length - 1) {
              // Move to the next tag and highlight
              gameManager.nextTag();
            }
            gameManager.highlightTag(gameManager.currentTag);
            // We are not supposed to push this character (a pop was required)
          } else {
            // Selection animation wrong
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
            }, 1000);
            // Display message of error
            gameManager.assignValueToFeedback('You can not push' + " closing tags ");
          }
        }
        // Did not select the right tag when pushing
        else {
          // Selection animation wrong
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
          }, 1000);
          // Display message of error
          gameManager.assignValueToFeedback('Select the correct tag to push');
        }
      });

      // Pop Button Click----------------------------
      gameManager.player.buttonPop.click(function () {
        // We have reached the end of the task, no other actions are alowed
        if (gameManager.player.currentTask == gameManager.tasks.length - 1) {
          // Selection animation correct
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedback');
          }, 1000);
          gameManager.assignValueToFeedback('You reached the end of this task - Submit');
        }
        // Perfom a possible pop action only if the player selects the current tag to examine
        else if ($(gameManager.tag_element[gameManager.currentTag]).text() == player.currentTagSelected) {
          // Now we need to check if the task is a pop or not
          if (gameManager.tasks[gameManager.player.currentTask] == "pop") {
            // Selection animation correct
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedback');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedback');
            }, 1000);
            // Perform the pop
            gameManager.player.stack.pop();
            // Remove node to the visual stack
            gameManager.removeStackVisNode();
            // Display push message 
            gameManager.assignValueToFeedback('Tag ' + gameManager.htmlTagToText(gameManager.player.currentTagSelected) + " popped from the stack");
            // Set the next task for the player
            gameManager.player.nextTask();
            // Only if we have tags left we move
            if (gameManager.currentTag < gameManager.tagList.length - 1) {
              // Move to the next tag and highlight
              gameManager.nextTag();
            }
            gameManager.highlightTag(gameManager.currentTag);
            // Attempting pop but the closing tag does not match the top of the stack
          } else if (gameManager.tasks[gameManager.player.currentTask] == "popNoMatch") {
            // Selection animation wrong
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
            // Set the next task for the player
            gameManager.player.nextTask();
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
            }, 1000);
            // Display message of error
            gameManager.assignValueToFeedback('The tag ' + gameManager.htmlTagToText(gameManager.player.currentTagSelected) + " does not match the top of the stack");
            // Attempting pop but the stack is empty. (too many closing tags)
          } else if (gameManager.tasks[gameManager.player.currentTask] == "popEmptyStack") {
            // Selection animation wrong
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
            // Set the next task for the player
            gameManager.player.nextTask();
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
            }, 1000);
            // Display message of error
            gameManager.assignValueToFeedback('The stack is currently empty. Cannot pop');
            // We are not supposed to pop this character (a push was required)
          } else {
            // Selection animation wrong
            gameManager.hrLine.addClass('active-feedback');
            gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
            setTimeout(function () {
              gameManager.hrLine.removeClass('active-feedback');
              gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
            }, 1000);
            // Display message of error
            gameManager.assignValueToFeedback('You can not pop' + " opening tags ");
          }
        } // Did not select the right tag when popping
        else {
          // Selection animation wrong
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
          }, 1000);
          // Display message of error
          gameManager.assignValueToFeedback('Select the correct tag before pop');
        }

      });

      // Top Button Click------------------------------
      gameManager.player.buttonTop.click(function () {
        // If there is at least one element to Peek
        if (gameManager.player.stack.size > 0) {
          // Selection animation correct
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedback');
          }, 1000);
          // Top animation for the visual node
          $(gameManager.stack_list).find('li').first().addClass('activeNode');
          // Show the top value
          gameManager.showTop();
          setTimeout(function () {
            $(gameManager.stack_list).find('li').first().removeClass('activeNode');
          }, 2000);
          let topValue = gameManager.player.stack.peek();
          gameManager.assignValueToFeedback('The Stack top value is: ' + " ' " + gameManager.htmlTagToText(topValue) + " '");
        } else {
          // Selection animation wrong
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
          }, 1000);
          gameManager.assignValueToFeedback('The Stack is empty. There is no top');
        }
      });

      // IsEmpty Button Click----------------------------
      gameManager.player.buttonIsEmpty.click(function () {
        // If the Stack is empty
        if (gameManager.player.stack.size == 0) {
          // Selection animation wrong
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedbackWrong');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedbackWrong');
          }, 1000);
          gameManager.assignValueToFeedback('The stack is currently empty');
        } else {
          // Selection animation correct
          gameManager.hrLine.addClass('active-feedback');
          gameManager.hrLine.parent().addClass('highlightFeedback');
          setTimeout(function () {
            gameManager.hrLine.removeClass('active-feedback');
            gameManager.hrLine.parent().removeClass('highlightFeedback');
          }, 1000);
          gameManager.assignValueToFeedback('The stack is not empty');
        }
      });

      // Check answer yes------------------------------------
      gameManager.player.buttonAnswer[0].addEventListener('click', (e) => {
        gameManager.acceptHtmlAnswer(e);
      });

      // Check answer no-------------------------------------
      gameManager.player.buttonAnswer[1].addEventListener('click', (e) => {
        gameManager.rejectHtmlAnswer(e);
      });

      // Check answer cancel-------------------------------------
      gameManager.player.buttonAnswer[2].addEventListener('click', () => {
        // undo the decision if submitting the answer
        gameManager.undoHtmlAnswer();
      });

      // Restart Interactive Tutorial
      $('#guide-button').click(function () {
        stackValidator = new Stack();
        gameManager.newHtml(stackValidator);
        gameManager.generateHtmlStream(false);
        gameManager.validateHtmlStream();
        gameManager.assignValueToFeedback('');
        gameManager.highlightTag(gameManager.currentTag);
        // clear all tag selections highlight
        for (let index = 0; index < gameManager.tag_item_text.length; index++) {
          $(gameManager.tag_item_text[index]).parent().removeClass('current-selection');
        }
        // Restart interactive tutorial task 
        currentTutorialCount = 13;
        tutorial[currentTutorialCount].set();
      });
    }
  });
};