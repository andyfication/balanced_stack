// Class Stack representing the data structure
class Stack {

  constructor() {
    this.top = null;
    this.size = 0;
  }
  // Push a value to the stack 
  push(value) {
    let node = new StackNode(value, this.top);
    this.top = node;
    this.size++;
  }
  // Pop the top value from the stack 
  pop() {
    let value = null;
    if (this.top) {
      value = this.top.value;
      this.top = this.top.nextNode;
      this.size--;
    } else {
      value = 'Empty Stack';
      console.log(this.size);
    }
    return value;
  }

  // Peek the top value without removing it from the Stack 
  peek() {
    let value = null;
    if (this.top) {
      value = this.top.value
    }
    // Return the top value 
    return value;
  }
  // Print the stack values
  print() {
    let ptr = this.top;
    while (ptr) {
      console.log(ptr.value);
      ptr = ptr.nextNode;
    }
    console.log('-----');
  }
}