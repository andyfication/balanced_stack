// Global variables
let currentTutorialCount = 0;
let beginGame = false;
let beginTutorial = false;
let comingFromTutorial = false;
let tutorial;
let gameManager;
let stackValidator;
let player;

// Tutorial exercise
var htmlTutorialExercise = ['<html> <head> <title> Tutorial </title> </head> <body> <h1> Tutorial </h1> <p> Stacks and html can work together </p> </body> </html>'];
// Tutorial exercise whithout rendering characters 
var htmlTutorialExerciseClean = ['<h1> Tutorial </h1> <p> Stacks and html can work together </p>'];

// Container of exercises
var htmlExercisesContainer = ['<body> <h1> Breakfast: </h1> <h2> Menu: </h2> <ul> <li> Fruits </li> <li> Cereals </li> <li> Porridge </li> <li> Tea </li> </ul> </body>',
  '<html> <body> <h1> Paragraphs: </h1> <p> Normal paragraph </p> <p> <strong> Bold paragraph </strong> </p> <p> <em> Emphasized paragraph </em> </p> </body> </html>',
  '<head> <title> Headings </title> </head> <body> <p> &ltimg id = "heading-img" src = "images/headings.gif"&gt </p> <h1> This is the h1 font size </h1> <h2> This is the h2 font size </h2> </body>',
  '<html> <body> <h1> Containers: </h1> <div> <p> I am inside the div container </p> </div> <p> I am outside of the div container </p> </body> </html>',
  '<body> <h1> Lemonde Recipe: </h1> <div> <ol> <li> 3 unwaxed lemons, roughly chopped </li> <li> 140g caster sugar </li> <li> 1l cold water </li> </ol> </div> </body>',
  '<html> <body> <h1> Block vs Inline </h1> <p> Paragraph tags are block elements </p> <p> <span> Span tags are inline elements </span> </p> </body> </html>',
  '<html> <body> <p> &ltimg id = "book-img" src = "images/book.gif"&gt </p> <h1> Book Title </h1> <h2> Book Chapter </h2> <p> Book Review </p> <p> <strong> Detailed Book Review </strong> </p> </body> </html>',
  '<h1> Lists: </h1> <p> Unordered List </p> <ul> <li> Dog </li> </ul> <p> Ordered List </p> <ol> <li> First Animal </li> </ol>',
  '<html> <head> <title> The page title </title> </head> <body> <h1> The main header </h1> <p> A text paragraph </p> <p> Another text paragraph </p> </body> </html>',
  '<html> <head> <title> Website title </title> </head> <body> <div> <p> Website content </p> </div> </body> </html>',

  '<body> <h1> Breakfast: </h1> <h2> Menu: </h2> <ul> <li> Fruits </li> <li> Cereals </li> <li> Porridge </li> <li> Tea </li> </ul>',
  '<html> <body> <h1> Paragraphs: </h1> <p> Normal paragraph </p> <p> <strong> Bold paragraph </strong> </p> <p> <em> Emphasized paragraph </p> </body> </html>',
  '<head> <title> Headings </title> </head> <body> <p> &ltimg id = "heading-img" src = "images/headings.gif"&gt </p> <h1> This is the h1 font size <h1> </h1> <h2> This is the h2 font size </h2> </body>',
  '<body> <h1> Containers: </h1> <div> <p> I am inside the div container </p> </div> <p> I am outside of the div container </p> </body> </html>',
  '<body> <h1> Lemonde Recipe: </h1> <div> <ol> <li> 3 unwaxed lemons, roughly chopped </li> <li> 140g caster sugar </li> <li> 1l cold water </li> </ul> </div> </body>',
  '<html> <body> <h1> Block vs Inline </h2> <p> Paragraph tags are block elements </p> <p> <span> Span tags are inline elements </span> </p> </body> </html>',
  '<html> <body> <p> &ltimg id = "book-img" src = "images/book.gif"&gt </p> <h1> Book Title </h1> <h2> Book Chapter </h2> <p> Book Review </p> <p> <strong> Detailed Book Review <strong> </p> </body> </html>',
  '<h1> Lists: </h1> <p> Unordered List </p> <ul> <li> Dog </li> </ul> <p> Ordered List </p> </ol> <li> First Animal </li> <ol>',
  '<html> <head> <title> The page title </title> <body> <h1> The main header </h1> <p> A text paragraph </p> <p> Another text paragraph </p> </body> </html>',
  '<html> <head> <title> Website title </title> </head> <body> <div> <p> <p> Website content </p> </div> </body> </html>'
];

// Container of exercises  whithout rendering characters 

var htmlExercisesContainerClean = ['<h1> Breakfast: </h1> <h2> Menu: </h2> <ul> <li> Fruits </li> <li> Cereals </li> <li> Porridge </li> <li> Tea </li> </ul>',
  '<h1> Paragraphs: </h1> <p> Normal paragraph </p> <p> <strong> Bold paragraph </strong> </p> <p> <em> Emphasized paragraph </em> </p>',
  '<p> <img id = "heading-img" src = "images/headings.gif"> </p> <h1> This is the h1 font size </h1> <h2> This is the h2 font size </h2>',
  '<h1> Containers: </h1> <div> <p> I am inside the div container </p> </div> <p> I am outside of the div container </p>',
  '<h1> Lemonde Recipe: </h1> <div> <ol> <li> 3 unwaxed lemons, roughly chopped </li> <li> 140g caster sugar </li> <li> 1l cold water </li> </ol> </div>',
  '<h1> Block vs Inline </h1> <p> Paragraph tags are block elements </p> <p> <span> Span tags are inline elements </p> </span>',
  '<p> <img id = "book-img" src = "images/book.gif"> <h1> Book Title </h1> <h2> Book Chapter </h2> <p> Book Review </p> <p> <strong> Detailed Book Review </strong> </p>',
  '<h1> Lists: </h1> <p> Unordered List </p> <ul> <li> Dog </li> </ul> <p> Ordered List </p> <ol> <li> First Animal </li> </ol>',
  '<h1> The main header </h1> <p> A text paragraph </p> <p> Another text paragraph </p>',
  '<div> <p> Website content </p> </div>',

  '<h1> Breakfast: </h1> <h2> Menu: </h2> <ul> <li> Fruits </li> <li> Cereals </li> <li> Porridge </li> <li> Tea </li> </ul>',
  '<h1> Paragraphs: </h1> <p> Normal paragraph </p> <p> <strong> Bold paragraph </strong> </p> <p> <em> Emphasized paragraph </p>',
  '<p> <img id = "heading-img" src = "images/headings.gif"> <h1> This is the h1 font size <h1> </h1> <h2> This is the h2 font size </h2>',
  '<h1> Containers: </h1> <div> <p> I am inside the div container </p> </div> <p> I am outside of the div container </p>',
  '<h1> Lemonde Recipe: </h1> <div> <ol> <li> 3 unwaxed lemons, roughly chopped </li> <li> 140g caster sugar </li> <li> 1l cold water </li> </ul> </div>',
  '<h1> Block vs Inline </h2> <p> Paragraph tags are block elements </p> <p> <span> Span tags are inline elements </span> </p>',
  '<p> <img id = "book-img" src = "images/book.gif"> <h1> Book Title </h1> <h2> Book Chapter </h2> <p> Book Review </p> <p> <strong> Detailed Book Review <strong> </p>',
  '<h1> Lists: </h1> <p> Unordered List </p> <ul> <li> Dog </li> </ul> <p> Ordered List </p> </ol> <li> First Animal </li> <ol>',
  '<h1> The main header </h1> <p> A text paragraph </p> <p> Another text paragraph </p> </body> </html>',
  '<div> <p> <p> Website content </p> </div>'
];



$(document).ready(() => {

  // Tutorial Object, has text to display and a function set to change the state of the content
  tutorial = [{
      text: "Welcome, I'll be your task manager and guide <button class = 'button-next'> Next</button> <button class = 'button-skip'>Skip</button>",
      index: 0,
      set: function () {
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can find the html code generator<button class = 'button-next'>Next</button>",
      index: 1,
      set: function () {
        $('#html-grid-transparent').css('display', 'none');
        $('#html-grid').addClass('highlightBorder');
        $('#html-grid-transparent').addClass('non-active');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can find the stack interaction panel<button class = 'button-next'>Next</button>",
      index: 2,
      set: function () {
        $('#html-grid').removeClass('highlightBorder');
        $('#tag-grid').addClass('highlightBorder');
        $('#tag-grid').addClass('non-active');
        $('#tag-grid-transparent').css('display', 'none');
        $('#html-grid-transparent').css('display', 'block');
        $('#stack-tag-list-container-transparent').css('display', 'none');
        $('#stack-buttons-container-transparent').css('display', 'none');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can view and select start and close html tags <button class = 'button-next'>Next</button>",
      index: 3,
      set: function () {
        $('#tag-grid').removeClass('highlightBorder');
        $('#stack-buttons-container-transparent').css('display', 'block');
        $('#interactive-buttons-stack-transparent').css('display', 'none');
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can interact with the stack and check feedback<button class = 'button-next'>Next</button>",
      index: 4,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#stack-tag-list-container-transparent').css('display', 'block');
        $('#stack-buttons-container-transparent').css('display', 'none');
        $('#stack-buttons-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);

      }
    },
    {
      text: "Here you can view the stack current status<button class = 'button-next'>Next</button>",
      index: 5,
      set: function () {
        $('#stack-buttons-container').removeClass('highlightBorder');
        $('#tag-grid-transparent').css('display', 'block');
        $('#stack-buttons-container-transparent').css('display', 'block');
        $('#stack-display-grid-transparent').css('display', 'none');
        $('#stack-display-grid').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "You can use the [Reset Stack] button to reset the current exercise <button class = 'button-next'>Next</button>",
      index: 6,
      set: function () {
        $('#stack-display-grid-transparent').css('display', 'block');
        $('#stack-display-grid').removeClass('highlightBorder');
        $('#reset-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "You can use the [Random Html] button to generate a new exercise <button class = 'button-next'>Next</button>",
      index: 7,
      set: function () {
        $('#reset-button').removeClass('highlightBorder');
        $('#random-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: " Finally you can use the [Help] button to review this information <button class = 'button-next'>Next</button>",
      index: 8,
      set: function () {
        $('#random-button').removeClass('highlightBorder');
        $('#help').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Let me guide your first attempt<button class = 'button-next'>Ok</button> <button class = 'button-skip'>Skip</button>",
      index: 9,
      set: function () {
        $('#help').removeClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Use the stack to check whether the html code is well formed<button class = 'button-next'>Begin</button> ",
      index: 10,
      set: function () {
        $('#stack-feedback').html('Select one of the actions');
        $('#tag-grid').removeClass('non-active');
        $('#tag-grid').addClass('active');
        $('#html-grid-transparent').css('display', 'block');
        $('#tag-grid-transparent').css('display', 'block');
        $('#stack-buttons-container-transparent').css('display', 'block');
        $('#stack-display-grid-transparent').css('display', 'block');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Submit your answer when you have the solution<button class = 'button-next'>Submit</button> ",
      index: 11,
      set: function () {
        // Not coming from the tutorial section 
        if (!comingFromTutorial) {
          $('#html-grid-transparent').css('display', 'none');
          $('#tag-grid-transparent').css('display', 'none');
          $('#stack-tag-list-container-transparent').css('display', 'none');
          $('#stack-buttons-container-transparent').css('display', 'none');
          $('#stack-display-grid-transparent').css('display', 'none');
          $('#push-button').removeClass('non-active');
          $('#pop-button').removeClass('non-active');
          $('#top-button').removeClass('non-active');
          $('#empty-button').removeClass('non-active');
          let arrayElement = $('.tag-item');
          arrayElement.removeClass('non-active');
          $('#suggestion').html(this.text);
          // We can start the Game
          beginGame = true;
          beginTutorial = false;
          comingFromTutorial = false;
          // Coming from the tutorial section 
        } else {
          console.log('From tutorial');
          $('#suggestion').html("Submit your answer now <button class = 'button-next'>Submit</button> ");
          $('#pop-button').removeClass('highlightBorder');
          $('#pop-button').addClass('non-active');
          let arrayElement = $('.tag-item');
          arrayElement.addClass('non-active');
        }
      }
    },
    {
      text: "Is the given html well formed ?",
      index: 12,
      set: function () {
        if (!comingFromTutorial) {
          $('.answer').css('display', 'inline-block');
        } else {
          $('.answer').css('display', 'inline-block');
          $($('.answer')[2]).css('display', 'none');
          comingFromTutorial = false;
        }
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "The 'HTML Code' section highlights the html tag to examine<button class = 'button-next'>Next</button> ",
      index: 13,
      set: function () {
        beginGame = true;
        beginTutorial = true;
        comingFromTutorial = true;
        $('#tag-grid').removeClass('non-active');
        $('#reset-button').addClass('non-active');
        $('#random-button').addClass('non-active');
        $('#stack-tag-container').addClass('non-active');
        $('#html-grid-transparent').css('display', 'none');
        $('#tag-grid-transparent').css('display', 'none');
        $('#stack-display-grid-transparent').css('display', 'none');
        $('#stack-tag-list-container-transparent').css('display', 'none');
        $('#stack-buttons-container-transparent').css('display', 'none');
        $('#html-grid').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 14,
      set: function () {
        $('#html-grid').removeClass('highlightBorder');
        $('#stack-tag-container').removeClass('non-active');
        $('#stack-buttons-container').addClass('non-active');
        $('#stack-tag-list-container').addClass('highlightBorder');

        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[0]).removeClass('non-active');
        $(arrayElement[0]).one('click', function () {
          currentTutorialCount = 15;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Here you can check feedback on your actions<button class = 'button-next'>Next</button> ",
      index: 15,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#stack-feedback-container').addClass('highlightBorder');
        let arrayElement = $('.tag-item');
        $(arrayElement[0]).addClass('non-active');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is an opening html tag. Push it to the stack",
      index: 16,
      set: function () {
        $('#stack-feedback-container').removeClass('highlightBorder');
        $('#stack-buttons-container').removeClass('non-active');
        $('#top-button').addClass('non-active');
        $('#pop-button').addClass('non-active');
        $('#push-button').removeClass('non-active');
        $('#push-button').addClass('highlightBorder');
        $('#empty-button').addClass('non-active');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 17;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "The tag was added to the stack. Here you can visualize the stack <button class = 'button-next'>Next</button> ",
      index: 17,
      set: function () {
        $('#push-button').removeClass('highlightBorder');
        $('#push-button').addClass('non-active');
        $('#stack-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Notice that the highighted tag has changed in the 'HTML Code'<button class = 'button-next'>Next</button> ",
      index: 18,
      set: function () {
        $('#stack-container').removeClass('highlightBorder');
        $('#html-grid').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 19,
      set: function () {
        $('#html-grid').removeClass('highlightBorder');
        $('#stack-tag-list-container').addClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[1]).removeClass('non-active');
        $(arrayElement[1]).one('click', function () {
          currentTutorialCount = 20;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is an opening html tag. Push it to the stack",
      index: 20,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#push-button').removeClass('non-active');
        $('#push-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 21;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 21,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#push-button').addClass('non-active');
        $('#push-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[2]).removeClass('non-active');
        $(arrayElement[2]).one('click', function () {
          currentTutorialCount = 22;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is an opening html tag. Push it to the stack",
      index: 22,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#push-button').removeClass('non-active');
        $('#push-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 23;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 23,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#push-button').addClass('non-active');
        $('#push-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[3]).removeClass('non-active');
        $(arrayElement[3]).one('click', function () {
          currentTutorialCount = 24;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is a closing html tag. Pop it from the stack",
      index: 24,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#pop-button').removeClass('non-active');
        $('#pop-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 25;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "The feedback area confirmed a valid pop action<button class = 'button-next'>Next</button> ",
      index: 25,
      set: function () {
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $('#pop-button').addClass('non-active');
        $('#pop-button').removeClass('highlightBorder');
        $('#stack-feedback-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 26,
      set: function () {
        $('#stack-feedback-container').removeClass('highlightBorder');
        $('#stack-tag-list-container').addClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[4]).removeClass('non-active');
        $(arrayElement[4]).one('click', function () {
          currentTutorialCount = 27;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is a closing html tag. Pop it from the stack",
      index: 27,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#pop-button').removeClass('non-active');
        $('#pop-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 28;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 28,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#pop-button').addClass('non-active');
        $('#pop-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[5]).removeClass('non-active');
        $(arrayElement[5]).one('click', function () {
          currentTutorialCount = 29;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is an opening html tag. Push it to the stack",
      index: 29,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#push-button').removeClass('non-active');
        $('#push-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 30;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 30,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#push-button').addClass('non-active');
        $('#push-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[6]).removeClass('non-active');
        $(arrayElement[6]).one('click', function () {
          currentTutorialCount = 31;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is an opening html tag. Push it to the stack",
      index: 31,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#push-button').removeClass('non-active');
        $('#push-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 32;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 32,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#push-button').addClass('non-active');
        $('#push-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[7]).removeClass('non-active');
        $(arrayElement[7]).one('click', function () {
          currentTutorialCount = 33;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is a closing html tag. Pop it from the stack",
      index: 33,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#pop-button').removeClass('non-active');
        $('#pop-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 34;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 34,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#pop-button').addClass('non-active');
        $('#pop-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[8]).removeClass('non-active');
        $(arrayElement[8]).one('click', function () {
          currentTutorialCount = 35;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is an opening html tag. Push it to the stack",
      index: 35,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#push-button').removeClass('non-active');
        $('#push-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#push-button').one('click', function () {
          currentTutorialCount = 36;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 36,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#push-button').addClass('non-active');
        $('#push-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[9]).removeClass('non-active');
        $(arrayElement[9]).one('click', function () {
          currentTutorialCount = 37;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is a closing html tag. Pop it from the stack",
      index: 37,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#pop-button').removeClass('non-active');
        $('#pop-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 38;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Let's check the top of the stack",
      index: 38,
      set: function () {
        $('#pop-button').addClass('non-active');
        $('#pop-button').removeClass('highlightBorder');
        $('#top-button').removeClass('non-active');
        $('#top-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#top-button').one('click', function () {
          currentTutorialCount = 39;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "The feedback area showed the stack top value<button class = 'button-next'>Next</button> ",
      index: 39,
      set: function () {
        $('#top-button').addClass('non-active');
        $('#top-button').removeClass('highlightBorder');
        $('#stack-feedback-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Let's now check if the stack is empty",
      index: 40,
      set: function () {
        $('#empty-button').removeClass('non-active');
        $('#empty-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#empty-button').one('click', function () {
          currentTutorialCount = 41;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "The feedback area showed that the stack is not empty<button class = 'button-next'>Next</button> ",
      index: 41,
      set: function () {
        $('#empty-button').addClass('non-active');
        $('#empty-button').removeClass('highlightBorder');
        $('#stack-feedback-container').addClass('highlightBorder');
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 42,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#stack-feedback-container').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[10]).removeClass('non-active');
        $(arrayElement[10]).one('click', function () {
          currentTutorialCount = 43;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is a closing html tag. Pop it from the stack",
      index: 43,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#pop-button').removeClass('non-active');
        $('#pop-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 44;
          tutorial[currentTutorialCount].set();
        });
      }
    },
    {
      text: "Now select the matching highlighted tag in the 'HTML Tags' section",
      index: 44,
      set: function () {
        $('#stack-tag-list-container').addClass('highlightBorder');
        $('#stack-feedback-container').removeClass('highlightBorder');
        $('#pop-button').addClass('non-active');
        $('#pop-button').removeClass('highlightBorder');
        let arrayElement = $('.tag-item');
        arrayElement.addClass('non-active');
        $(arrayElement[11]).removeClass('non-active');
        $(arrayElement[11]).one('click', function () {
          currentTutorialCount = 45;
          tutorial[currentTutorialCount].set();
        });
        $('#suggestion').html(this.text);
      }
    },
    {
      text: "It is a closing html tag. Pop it from the stack",
      index: 45,
      set: function () {
        $('#stack-tag-list-container').removeClass('highlightBorder');
        $('#pop-button').removeClass('non-active');
        $('#pop-button').addClass('highlightBorder');
        $('#suggestion').html(this.text);
        $('#pop-button').one('click', function () {
          currentTutorialCount = 11;
          tutorial[currentTutorialCount].set();
        });
      }
    }
  ];

  // Setting the first tutorial task
  tutorial[currentTutorialCount].set();


  // Increment tutorial task on button click 
  $('#suggestion').on('click', '.button-next', () => {
    // if click on tutorial begin tutorial 
    if (currentTutorialCount == 9) {
      currentTutorialCount = 13;
      tutorial[currentTutorialCount].set();
    } else {
      // Normal increment of tasks 
      currentTutorialCount += 1;
      tutorial[currentTutorialCount].set();
    }

  });

  //Jump the tutorial section 
  $('#suggestion').on('click', '.button-skip', () => {
    // Skip the intro tutorial
    if (currentTutorialCount == 0) {
      currentTutorialCount = 9;
      tutorial[currentTutorialCount].set();
      // Skip the guided tutorial 
    } else if (currentTutorialCount == 9) {
      currentTutorialCount = 10;
      tutorial[currentTutorialCount].set();
    }
  });
  // Toggle the Graph Legenda on help button click 
  $('#help').on('click', () => {
    $(".modal-title").html('Balanced Stack');
    $(".modal-body").html('<h3 style="margin-top:15px">Overview</h3><p> Use the <strong>Stack</strong> data structure to determine if a given html code is <strong>well formed</strong> or not. Rememebr that elements that enter the <strong>Stack</strong> first are the last to leave. Once you execute all the steps, you will end up with either an empty <strong>Stack</strong> (meaning that the html code is well formed) or a non-empty <strong>Stack</strong> (meaning that the html code is not well formed). <br /> <h3>How to Play</h3> <p style = "font-size:20px; margin-top: 20px; font-weight: bold"> Html Code: <p>Find the highlighted html tag in the "Html Code" section. This corresponds to the current html tag to examine.</p> <img src = "images/html_stream.gif" id ="html-stream-img"/> <br /> <p style = "font-size:20px; font-weight: bold"> Tag Selection:</p><p>Go to the "Html start and close tags" section and select the tag which corresponds to the highlighted tag in the "Html Code" section.</p> <img src = "images/html_tag.gif" id ="html-tag-img"/> <br /> <p style = "font-size:20px; font-weight: bold"> Operations:</p> <p> Once you have selected the correct tag, use the buttons “push”, “pop”, “top” or “is empty ?” to manipulate the stack data structure. </p> <img src = "images/buttons.gif" id ="buttons-img"/> <br />   <p style = "font-size:16px; font-weight: bold"> Push: <span style = "font-size:16px; font-weight: normal">Add the element at the top of the Stack</span></p><p style = "font-size:16px; font-weight: bold"> Pop: <span style = "font-size:16px; font-weight: normal">Remove and store the top element from the Stack</span></p><p style = "font-size:16px; font-weight: bold"> Top: <span style = "font-size:16px; font-weight: normal">View the top element of the Stack</span></p><p style = "font-size:16px; font-weight: bold"> Is Empty: <span style = "font-size:16px; font-weight: normal">Check if the Stack contains any elements</span></p><p>The rules to use a stack to check whether some html code is well formed are relatively simple:</p><p>- Examine the html tags in order of appearance</p><p>- Push to the stack opening html tags</p><p>- Pop from the stack closing html tags </p><p>Once you have examined all the html tags, if the stack is empty it means that the html code is well formed. On the other hand, if the stack is not empty it means that the html code is not well formed.</p><p>When you perform the pop operation, the algorithm checks if the opening html tag at the top of the stack matches with the closing html tag you are examining; the pop happens only if they match. If the pop fails, it means that the html code is not well formed.</p><p>If you perform a pop operation and the stack is empty, it means that there are not enough opening tags and therefore the html code is not well formed.</p><p style = "font-size:20px; font-weight: bold"> Submission:</p><p>When you have the solution, use the <img src = "images/submit.gif" id ="submit-img"/> button to validate your answer. Remember that you also need to correctly use the stack data strucure to succesfully complete the exercise.</p>');
    $(".modal").modal("show");
  });

  // Activete interactive guide button
  $('#closeModal').one('click', () => {
    $('#guide-button').css('display', 'block');
  });

  // Activete interactive guide button
  $('#close-x').one('click', () => {
    $('#guide-button').css('display', 'block');
  });
});