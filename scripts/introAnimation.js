var mainAimationFinished = false;
var showTutorial = false;
var $mainContainer;

$(document).ready(function () {
  var $introContainer = $('#introContainer');
  $mainContainer = $('#main-container-grid');
  var $beginButton = $('#begin');
  var $introImageActive = $('#introImageActive');
  var $introImgagePassive = $('#introImage');
  // Start animation on begin button
  $beginButton.on('click', function () {
    $introImgagePassive.fadeOut(2000, function () {
      $introImageActive.fadeIn(1000, function () {
        $introContainer.fadeOut(2000, function () {
          $mainContainer.css('display', 'grid');
          $mainContainer.fadeIn(3000, function () {
            // end of animation 
            mainAimationFinished = true;
          });
        });
      });
    });
  })
})
// Once the animation ends, start the game container 
var waitingForEndAnimation = setInterval(function () {
  console.log(mainAimationFinished);
  if (mainAimationFinished) {
    $mainContainer.css('visibility', 'visible');
    $('.modal').modal("show");
    showTutorial = true;
    clearInterval(waitingForEndAnimation);
  }
}, 1000);